void main() {
  // basic program that stores and then prints the following data: Your name, favorite app, and city;
  //program starting
  print(
      "**********************************************************************");
  String fullName = "Ngandu Kennedy Bonianga";
  String favoriteApp = "Standard bank Shyft";
  String city = "Johannesburg";
  String message =
      'My name is $fullName.\nI stay in $city, and my favorite app is "$favoriteApp".';
  print(message);
  print(
      "**********************************************************************");
  //Program ending
}
