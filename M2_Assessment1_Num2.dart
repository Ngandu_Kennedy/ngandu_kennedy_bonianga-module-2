void main() {//program starts
  print("**********************************************************");
  //Create an array that stores all the winning apps of the MTN Business App of the Year Awards since 2012
  Map winning_Apps = {
    'Ambani': 2021,
    'EasyEquities': 2020,
    'Naked Insurance': 2019,
    'Khula': 2018,
    'Standard Bank Shyft': 2017,
    'Domestly': 2016,
    'WumDrop': 2015,
    'Live Inspect': 2014,
    'SnapScan': 2013,
    'Fnb': 2012
  };
  // Sorting and printing the apps by name;
  winning_Apps.forEach((key, value) {
    print("Name Of The App: $key.");
  });
  print("-------------------------------------------------------");
  //Printing the winning app of 2017 and the winning app of 2018.;
  winning_Apps.forEach((key, value) {
    if (value == 2017 || value == 2018) {
      print("Winning App Name:$key       Year:$value");
    }
  });
  print("-------------------------------------------------------");
  //Print the total number of apps from the array.
  int count = 0;
  for (int x = 0; x < winning_Apps.length; x++) count++;
  {
    print('The Total number of Apps from the collection is "$count".');
  }
  print("**********************************************************");
}
//program ends
