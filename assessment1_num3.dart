void main() {
  print("*" * 141);
  App winner = new App();
  // Creating a function that transform the app name to all capital letters.
  winner.appName = "Shyft for Standard Bank";
  winner.category = "Best Financial Solution";
  winner.developer = "Shyft team";
  winner.year = 2017;
  var cap = winner.capitalize(winner.appName);
  print(
      '$cap application developed by ${winner.developer} was announced as the ${winner.category} of MTN app of the year edition ${winner.year}.');

  print("*" * 141);
}

class App {
  var appName;
  var category;
  var developer;
  var year;
  String capitalize(var name) {
    name = this.appName.toUpperCase();
    return name;
  }
}
